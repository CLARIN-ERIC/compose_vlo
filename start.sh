#!/usr/bin/env bash
BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
. "${BASE_DIR}/script/_inc.sh"

COMPOSE_OPTS="${COMPOSE_OPTS}"
COMPOSE_CMD_ARGS="${COMPOSE_CMD_ARGS}"

if [ -z "${COMPOSE_OPTS}" ] && [ "$1" ]; then
	info "COMPOSE_OPTS not set but argument provided; control script version < 3.0.0 ?"
	# provide some backward compatibility with control script 2.x
	COMPOSE_OPTS="$1"
	if [ -z "${COMPOSE_CMD_ARGS}" ]; then
		COMPOSE_CMD_ARGS=("-d")
		if [ "$2" ]; then
			COMPOSE_CMD_ARGS+=("$2")
		fi
	fi
fi

if [ -z "${COMPOSE_OPTS}" ] || [ -z "${COMPOSE_CMD_ARGS}" ]; then
	warn "COMPOSE_OPTS and/or COMPOSE_CMD_ARGS not set!"
fi

info "Start.sh -- Arguments: $@"
info "Start.sh -- COMPOSE_OPTS: ${COMPOSE_OPTS}"
info "Start.sh -- COMPOSE_CMD_ARGS: ${COMPOSE_CMD_ARGS}"

start_vlo_main() {
	create_missing_volume "vlo-data"
		
	create_missing_network "network_linkchecker" "--internal"

	if check_service; then
		warn "Service already appears to be running, will not try to remove home provisioning volume"
	else
		remove_solr_home_provisioning_volume
	fi
	dcompose ${COMPOSE_OPTS} up ${COMPOSE_CMD_ARGS}

}

create_missing_volume() {
	VOLUME="$1"
	shift
	OPTIONS="$*"
	if [ "${VOLUME}" ] && [ "$(docker volume ls |grep -c "${VOLUME}")"  -eq 0 ]; then	
		echo "Creating missing volume '${VOLUME}'"
		if [ "${OPTIONS}" ]  && [ "${#OPTIONS[@]}" ]; then
			docker volume create "${OPTIONS[@]}" -- "${VOLUME}"
		else
	        docker volume create -- "${VOLUME}"
	    fi
    fi

}

create_missing_network() {
	NETWORK="$1"
	shift
	OPTIONS="$*"
	if [ "${NETWORK}" ] && [ "$(docker network ls |grep -c "${NETWORK}")"  -eq 0 ]; then
		echo "Creating missing network '${NETWORK}'"
		if [ "${OPTIONS}" ]  && [ "${#OPTIONS[@]}" ]; then
	        docker network create "${OPTIONS[@]}" -- "${NETWORK}"
	    else
		    docker network create -- "${NETWORK}"
		fi
    fi
}

remove_solr_home_provisioning_volume() {
	info "Trying to remove solr home provisioning volume...."
	eval "$(grep "COMPOSE_PROJECT_NAME" "${VLO_COMPOSE_DIR}/.env")"
	if [ "${COMPOSE_PROJECT_NAME}" ]; then
		VOLUME_NAME="${COMPOSE_PROJECT_NAME}_${SOLR_HOME_PROVISIONING_VOLUME_NAME}"
		if docker volume ls | grep -E "${VOLUME_NAME}$"; then
			ACTUAL_VOLUME_NAME=$(docker volume ls | grep -E -o "${VOLUME_NAME}$")
			echo -n "Remove volume ${ACTUAL_VOLUME_NAME}... "
			if docker volume rm "${ACTUAL_VOLUME_NAME}" > /dev/null; then
				info "done"
			else
				fatal "FAILED!"
				exit 1
			fi
		else
			info "No Solr home provisioning volume found"
		fi
	else
		warn "Could not determine compose project name. Solr home provisioning will NOT be cleaned up!"	
	fi
}

start_vlo_main "$@"
