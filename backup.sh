#!/usr/bin/env bash
BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
. "${BASE_DIR}/script/_inc.sh"

set -e

VERBOSE=0

# Parse options
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
	-v|--verbose)
		VERBOSE=1
		;;
	*)
		ARG_REMAINDER_ARRAY+=("$key")
		;;
esac
shift
done

VLO_SOLR_CONTAINER="$(container_name "${VLO_SOLR_SERVICE}")"

main() {	
	log_debug "Checking for running service ${VLO_SOLR_SERVICE}"
	
	if service_is_running "${VLO_SOLR_SERVICE}"; then
		echo -e "VLO Solr is running. Starting backup procedure...\n"
	else
		echo "VLO Solr does not seem to be running. Please start the service and try again.."
		exit 1
	fi

	if [ "${BACKUPS_DIR}" ]; then
		log_debug "Backups directory (BACKUPS_DIR): ${BACKUPS_DIR}"
		BACKUP_DIR="$( cd "${BACKUPS_DIR}" >/dev/null && pwd )"
	else
		echo "FATAL: Environment variable BACKUPS_DIR not set"
		exit 1
	fi
	
	if ! exists_dir_in_container "${VLO_SOLR_CONTAINER}" "${CONTAINER_BACKUP_FILES_DIR}"; then
		echo "Backups directory not found in container. Please enable the overlay 'backup-restore' and try again!"
		exit 1
	fi
	
	export TARGET_DIR_HOST="${BACKUP_DIR}/work-backup"
	export TARGET_DIR_CONTAINER="${CONTAINER_BACKUP_FILES_DIR}/work-backup"
	
	log_debug "TARGET_DIR_HOST: ${TARGET_DIR_HOST}"
	
	export_credentials
	
	if ! (mkdir -p "${BACKUP_DIR}" && [ -d "${BACKUP_DIR}" ] && [ -x "${BACKUP_DIR}" ]); then
		echo "Cannot create and/or access backup directory ${BACKUP_DIR}"
		exit 1
	fi
	
	log_debug "Making directory inside container"
	(cd "${VLO_COMPOSE_DIR}" && 
		docker exec -t -u root "${VLO_SOLR_CONTAINER}" sh -c "mkdir -p ${CONTAINER_BACKUP_DIR} && chown -R solr:solr ${CONTAINER_BACKUP_DIR}")
	
	log_debug "Checking env"
	check_env

	echo "Backups directory: ${BACKUP_DIR}"
	echo "Backing up Solr index to ${TARGET_DIR_HOST}"
	echo "    (in container: ${TARGET_DIR_CONTAINER})"
	echo "Solr service: ${VLO_SOLR_SERVICE}"

	log_debug "Checking replication service"
	check_replication_service

	log_debug "Cleanup"
	cleanup_backup
	
	log_debug "Setting permissions"
	set_permissions

	log_debug "Carrying out backup"
	do_backup

	log_debug "Extracting backup from volume"
	extract_backup
	
	log_debug "Cleanup in container"
	remove_backup
	cleanup_backup
	
	if ! [ -d "${TARGET_DIR_HOST}" ]; then
		echo "Backup directory does not exist after backup. Failed backup?"
		exit 1
	fi
	
	echo "Compressing new backup..."
	COMPRESSED_BACKUP_FILE="${BACKUP_FILE_PREFIX}-$(date +%Y%m%d%H%M%S).tar.bz2"

	log_debug "Making archive ${COMPRESSED_BACKUP_TARGET_PATH}"
	log_debug "Contents to include: $(find "${TARGET_DIR_HOST}" -maxdepth 1 -mindepth 1 -type d)"
	
	if docker exec -u solr -w "${TARGET_DIR_CONTAINER}" "${VLO_SOLR_CONTAINER}" \
		find . -maxdepth 1 -mindepth 1 -type d -exec tar jcvf "/tmp/${COMPRESSED_BACKUP_FILE}" {} \;
	then
		docker exec -u root "${VLO_SOLR_CONTAINER}" mv "/tmp/${COMPRESSED_BACKUP_FILE}" "${CONTAINER_BACKUP_FILES_DIR}/${COMPRESSED_BACKUP_FILE}" \
		&& docker exec -u root  "${VLO_SOLR_CONTAINER}" chown "${UID}" "${CONTAINER_BACKUP_FILES_DIR}/${COMPRESSED_BACKUP_FILE}"
		
		BACKUP_RESULT_FILE="${BACKUPS_DIR}/${COMPRESSED_BACKUP_FILE}"
		
		if [ -e "${BACKUP_RESULT_FILE}" ]; then		
			echo "Backup file ${BACKUP_RESULT_FILE} has been created successfully!"
		
			echo "Cleaning up uncompressed backup..."
			remove_dir_in_container "${TARGET_DIR_CONTAINER}"
			
			echo "Done!"
			exit 0
		else
			echo "Fatal: expected to find ${BACKUP_RESULT_FILE} but file is not there"
		fi
	fi
	echo "Creation of backup archive failed. Cleaning up ${TARGET_DIR_HOST}."
	remove_dir_in_container "${TARGET_DIR_CONTAINER}"
	exit 1
}

remove_dir_in_container() {
	docker exec -t -u root "${VLO_SOLR_CONTAINER}" find "${1}" -maxdepth 0 -type d -exec rm -rf {} \;
}

check_env() {
	log_debug "Checking existence of .env in VLO_COMPOSE_DIR (${VLO_COMPOSE_DIR}/.env)"
	if ! [ -f "${VLO_COMPOSE_DIR}/.env" ]; then
		echo "Please set environment variable VLO_COMPOSE_DIR to an existing compose project directory"
		exit 1
	else
		log_debug "Passed"
	fi
	
	log_debug "Checking value of VLO_SOLR_INDEX_URL"
	if [ -z "$VLO_SOLR_INDEX_URL" ]; then
		echo "Please set environment variable VLO_SOLR_INDEX_URL"
		exit 1
	else
		log_debug "VLO_SOLR_INDEX_URL: ${VLO_SOLR_INDEX_URL}"
	fi

	log_debug "Checking values of VLO_SOLR_BACKUP_USERNAME and VLO_SOLR_BACKUP_PASSWORD"
	if [ -z "$VLO_SOLR_BACKUP_USERNAME" ] || [ -z "$VLO_SOLR_BACKUP_PASSWORD" ]; then
		echo "Please set environment variables VLO_SOLR_BACKUP_USERNAME and VLO_SOLR_BACKUP_PASSWORD"
		exit 1
	else
		log_debug "VLO_SOLR_BACKUP_USERNAME: ${VLO_SOLR_BACKUP_USERNAME}"
		log_debug "VLO_SOLR_BACKUP_PASSWORD: ${VLO_SOLR_BACKUP_PASSWORD}"
	fi
}

set_permissions() {
	echo -e "Setting target permission...\n"
	(cd $VLO_COMPOSE_DIR && \
		docker exec -t -u root "${VLO_SOLR_CONTAINER}" chown -R solr "${CONTAINER_BACKUP_DIR}" )
}

get_backup_status() {
	solr_api_get "${VLO_SOLR_INDEX_REMOTE_URL}/replication?command=details"
}

do_backup() {
	echo -e "\nCarrying out backup...\n"
	REQ_BACKUP="${VLO_SOLR_INDEX_REMOTE_URL}/replication?command=backup&location=${CONTAINER_BACKUP_DIR}&name=${BACKUP_NAME:-backup}"
	log_debug "Making request: ${REQ_BACKUP}"
	if solr_api_get "${REQ_BACKUP}"; then
		echo "Checking status..."
		SUCCESS="false"
		while [ "$SUCCESS" != "true" ]; do
			if get_backup_status | grep "success"; then
				SUCCESS="true"
			else
				if get_backup_status | grep "exception"; then
					echo "Exception occurred. Terminating..."
					remove_backup
					cleanup_backup
					exit 1
				else
					echo "Checking again in ${SOLR_REPLICATION_STATUS_RETRY_INTERVAL} seconds..."
					sleep "${SOLR_REPLICATION_STATUS_RETRY_INTERVAL}"
					if [ "1" == "${VERBOSE}" ]; then
						log_debug "Not successful (yet). Status: "
						get_backup_status
					fi
				fi
			fi
		done
	else
		echo "Failed to create backup!"
		cleanup_backup
		exit 5
	fi
	
	echo "Backup status: "
	get_backup_status
	
	echo "Waiting ${SOLR_REPLICATION_STATUS_RETRY_INTERVAL} seconds for index writer to wrap up"
	sleep "${SOLR_REPLICATION_STATUS_RETRY_INTERVAL}"
	
	echo "Final backup status: "
	get_backup_status

	echo -e "\nDone...\n"
}

extract_backup() {	
	(cd $VLO_COMPOSE_DIR && \
		docker exec -t "${VLO_SOLR_CONTAINER}" mv "${CONTAINER_BACKUP_DIR}" "${TARGET_DIR_CONTAINER}")

	log_debug "${TARGET_DIR_HOST}: "	
	log_debug "$(ls "${TARGET_DIR_HOST}")"
}

remove_backup() {
	echo -e "Deleting backup from volume...\n"
	
	if ! solr_api_get \
			"${VLO_SOLR_INDEX_REMOTE_URL}/replication?command=deletebackup&location=${CONTAINER_BACKUP_DIR}&name=${BACKUP_NAME:-backup}"; then
		log_debug "Could not delete backup. Already moved?" 
	fi
}

cleanup_backup() {
	echo -e "Cleaning up...\n"
	
	(cd $VLO_COMPOSE_DIR && \
		docker exec -t -u root "${VLO_SOLR_CONTAINER}" bash -c "if [ -d '${CONTAINER_BACKUP_DIR}' ]; then rm -rf ${CONTAINER_BACKUP_DIR}/*; fi")
}

log_debug() {
	if [ "1" == "${VERBOSE}" ]; then
		echo "[DEBUG] $@"
	fi
}

main $@
