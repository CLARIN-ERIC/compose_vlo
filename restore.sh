#!/usr/bin/env bash
BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
. "${BASE_DIR}/script/_inc.sh"

set -e

VERBOSE=0

# Parse options
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
	-v|--verbose)
		VERBOSE=1
		;;
	*)
		ARG_REMAINDER_ARRAY+=("$key")
		;;
esac
shift
done

VLO_SOLR_CONTAINER="$(container_name "${VLO_SOLR_SERVICE}")"

main() {	
	log_debug "Checking for running service ${VLO_SOLR_SERVICE}"
	
	if service_is_running ${VLO_SOLR_SERVICE}; then
		echo -e "VLO Solr is running. Starting restore procedure...\n"
	else
		echo "VLO Solr does not seem to be running. Please start the service and try again.."
		exit 1
	fi

	if [ "${BACKUPS_DIR}" ]; then
		log_debug "Backups directory (BACKUPS_DIR): ${BACKUPS_DIR}"
		BACKUP_DIR="$( cd "${BACKUPS_DIR}" >/dev/null && pwd )"
	else
		echo "FATAL: Environment variable BACKUPS_DIR not set"
		exit 1
	fi
	
	if ! exists_dir_in_container "${VLO_SOLR_CONTAINER}" "${CONTAINER_BACKUP_FILES_DIR}"; then
		echo "Backups directory not found in container. Please enable the overlay 'backup-restore' and try again!"
		exit 1
	fi
		
	export_credentials
	
	if ! ([ -d "${BACKUP_DIR}" ] && [ -x "${BACKUP_DIR}" ]); then
		echo "Cannot access backup directory ${BACKUP_DIR}"
		exit 1
	fi
	
	log_debug "Checking env"
	check_env

	echo "Backups directory: ${BACKUP_DIR}"
	echo "Backup to restore: $(basename "${RESTORE_FILE}")"
	echo "Solr service: ${VLO_SOLR_SERVICE}"

	log_debug "Checking replication service"
	check_replication_service
	
	log_debug "Cleanup"
	cleanup_backup
	
	log_debug "Setting permissions"
	set_permissions
	
	log_debug "Extracting ${RESTORE_FILE} in container"
	extract_in_container

	log_debug "Carrying out restore"
	do_restore

	log_debug "Cleanup"
	cleanup_backup
	
	echo "Done"
	
# 	echo "Compressing new backup..."
# 	COMPRESSED_BACKUP_FILE="${BACKUP_FILE_PREFIX}-$(date +%Y%m%d%H%M%S).tgz"
# 	(cd "${SOURCE_DIR_HOST}" && \
# 		tar zcf "${COMPRESSED_BACKUP_FILE}" *)
# 	if mv "${SOURCE_DIR_HOST}/${COMPRESSED_BACKUP_FILE}" "${BACKUP_DIR}/${COMPRESSED_BACKUP_FILE}"; then
# 		echo "Moved to ${BACKUP_DIR}/${COMPRESSED_BACKUP_FILE}. Cleaning up uncompressed backup..."
# 		_remove_dir "${SOURCE_DIR_HOST}"
# 		echo "Done!"
# 	else
# 		echo "Creation of backup archive failed. Target directory '${SOURCE_DIR_HOST}' left as is."
# 		exit 1
# 	fi
}

check_env() {
	if [ -z "${RESTORE_FILE}" ]; then
		echo "RESTORE_FILE variable not set - please run via control script or set manually"
		exit 1
	else
		log_debug "RESTORE_FILE: ${RESTORE_FILE}"
	fi

	log_debug "Checking existence of .env in VLO_COMPOSE_DIR (${VLO_COMPOSE_DIR}/.env)"
	if ! [ -f "${VLO_COMPOSE_DIR}/.env" ]; then
		echo "Please set environment variable VLO_COMPOSE_DIR to an existing compose project directory"
		exit 1
	else
		log_debug "Passed"
	fi
	
	log_debug "Checking value of VLO_SOLR_INDEX_URL"
	if [ -z "$VLO_SOLR_INDEX_URL" ]; then
		echo "Please set environment variable VLO_SOLR_INDEX_URL"
		exit 1
	else
		log_debug "VLO_SOLR_INDEX_URL: ${VLO_SOLR_INDEX_URL}"
	fi

	log_debug "Checking values of VLO_SOLR_BACKUP_USERNAME and VLO_SOLR_BACKUP_PASSWORD"
	if [ -z "$VLO_SOLR_BACKUP_USERNAME" ] || [ -z "$VLO_SOLR_BACKUP_PASSWORD" ]; then
		echo "Please set environment variables VLO_SOLR_BACKUP_USERNAME and VLO_SOLR_BACKUP_PASSWORD"
		exit 1
	else
		log_debug "VLO_SOLR_BACKUP_USERNAME: ${VLO_SOLR_BACKUP_USERNAME}"
		log_debug "VLO_SOLR_BACKUP_PASSWORD: ${VLO_SOLR_BACKUP_PASSWORD}"
	fi
}

set_permissions() {
	echo -e "Ensuring existence of target directory"
	(cd $VLO_COMPOSE_DIR && \
		docker exec -t -u root "${VLO_SOLR_CONTAINER}" mkdir -p "${CONTAINER_BACKUP_DIR}")


	echo -e "Setting target permission...\n"
	(cd $VLO_COMPOSE_DIR && \
		docker exec -t -u root "${VLO_SOLR_CONTAINER}" chown -R solr "${CONTAINER_BACKUP_DIR}" )
}

get_restore_status() {
	solr_api_get "${VLO_SOLR_INDEX_REMOTE_URL}/replication?command=restorestatus"
}

do_restore() {
	echo -e "\nCarrying out restore...\n"
	REQ_RESTORE="${VLO_SOLR_INDEX_REMOTE_URL}/replication?command=restore&location=${CONTAINER_BACKUP_DIR}&name=${BACKUP_NAME:-backup}"
	log_debug "Making request: ${REQ_RESTORE}"
	if solr_api_get "${REQ_RESTORE}"; then
		echo "Checking status..."
		SUCCESS="false"
		while [ "$SUCCESS" != "true" ]; do
			if get_restore_status | grep "success"; then
				SUCCESS="true"
			else
				if get_restore_status | grep "exception"; then
					echo "Exception occurred. Terminating..."
# 					cleanup_backup
					exit 1
				else
					echo "Checking again in ${SOLR_REPLICATION_STATUS_RETRY_INTERVAL:-5} seconds..."
					sleep "${SOLR_REPLICATION_STATUS_RETRY_INTERVAL:-5}"
					if [ "1" == "${VERBOSE}" ]; then
						log_debug "Not successful (yet). Status: "
						get_restore_status
					fi
				fi
			fi
		done
	else
		echo "Failed to create backup!"
		cleanup_backup
		exit 5
	fi
	
	echo "Final backup status: "
	get_restore_status

	echo -e "\nDone...\n"
}

cleanup_backup() {
	echo -e "Cleaning up in container...\n"
	
	(cd $VLO_COMPOSE_DIR && \
		docker exec -t -u solr "${VLO_SOLR_CONTAINER}" bash -c "if [ -d '${CONTAINER_BACKUP_DIR}' ]; then rm -rf ${CONTAINER_BACKUP_DIR}/*; fi")
}

extract_in_container() {
	RESTORE_FILE_NAME="$(basename $RESTORE_FILE)"
	if ! [ -e "${BACKUP_DIR}/${RESTORE_FILE_NAME}" ]; then
		echo "Fatal: backup file ${RESTORE_FILE_NAME} expected to be in backups directory ${BACKUPS_DIR}"
		exit 1	
	fi
	(cd $VLO_COMPOSE_DIR && \
		docker exec -t -u root -w "${CONTAINER_BACKUP_DIR}" "${VLO_SOLR_CONTAINER}" tar jxvf "${CONTAINER_BACKUP_FILES_DIR}/${RESTORE_FILE_NAME}")
}

log_debug() {
	if [ "1" == "${VERBOSE}" ]; then
		echo "[DEBUG] $@"
	fi
}

main $@

