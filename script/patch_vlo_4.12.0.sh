#!/usr/bin/env bash

set -e

[ -n "${CONTAINER_NAME}" ] || (echo "Fatal: CONTAINER_NAME not set" && exit 1)
# CONTAINER_NAME='prod_vlo-vlo-web-1'

[ -n "${VERSION}" ] || (echo "Fatal: VERSION not set" && exit 1)
# VERSION='4.12.1-alpha1'

set -x


DISTR="https://github.com/clarin-eric/VLO/releases/download/${VERSION}/vlo-${VERSION}-docker.tar.gz"
MONITOR_PATH="vlo-${VERSION}-docker/bin/monitor"
CONTAINER_MONITOR_PATH="/opt/vlo/bin/monitor"

TEMP_DIR="$(mktemp -d)"

echo "1. Retrieving JAR and extracting VLO monitor..."
(cd "${TEMP_DIR}"  && curl -sL "${DISTR}" | tar zxvf - "${MONITOR_PATH}")

echo "2. Copying patched files..."

docker cp "${TEMP_DIR}/${MONITOR_PATH}/start.sh" \
	"${CONTAINER_NAME}:${CONTAINER_MONITOR_PATH}"
	
docker cp "${TEMP_DIR}/${MONITOR_PATH}/vlo-monitor-${VERSION}.jar" \
	"${CONTAINER_NAME}:${CONTAINER_MONITOR_PATH}"

echo "3. Cleaning up temp dir ${TEMP_DIR}"
rm -rf "$TEMP_DIR"

echo "4. Removing old database"
docker exec "${CONTAINER_NAME}" rm '/opt/vlo-monitoring/vlo-monitoring.mv.db'

echo "5. Done!"
