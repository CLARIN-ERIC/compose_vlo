describe('Frontpage', () => {
    it('Renders all elements', () => {
        cy.visit('/');
        cy.contains('h1', 'CLARIN Virtual Language Observatory').should('be.visible');
        cy.contains('a', 'See all records').should('be.visible');
        cy.contains('a', 'Take a quick tour').should('be.visible');
        cy.contains('a', 'Help').should('be.visible');
        cy.contains('a', 'Search').should('be.visible');
        cy.contains('a', 'Contributors').should('be.visible');
    })
})